	"default colorscheme
" colorscheme zenburn
" colorscheme codeschool
" colorscheme solarized
"colorscheme github
" colorscheme textmate16
" colorscheme railscasts
" colorscheme lucius
" colorscheme stackoverflow
" colorscheme Tomorrow-Night-Eighties
colorscheme grb256
" light colorschemes
" colorscheme mac_classic
" colorscheme textmate
" colorscheme eclipse
" colorscheme ironman

" solarized-dark settings
" let g:solarized_termcolors=256
" set background=dark
" colorscheme solarized

" enable arrows in gvim/macvim
let g:NERDTreeDirArrows=1

"default macvim font
" set guifont=Monaco:h12
" set guifont=AnonymousPro:h12
" set guifont=Inconsolata:h14
" set guifont=DejaVuSansMono:h12

"window settings
  "disable toolbar
set guioptions-=T

"disable arrow keys to force yourself to use hjkl
" map <up> <nop>
" map <down> <nop>
" map <left> <nop>
" map <right> <nop>
" imap <up> <nop>
" imap <down> <nop>
" imap <left> <nop>
" imap <right> <nop>
" 
"enable arrow keys
map <up> <up>
map <down> <down>
map <left> <left>
map <right> <right>
imap <up> <up>
imap <down> <down>
imap <left> <left>
imap <right> <right>
